const { semanticReleaseConfigDockerMulti } = require('@beepbeepgo/semantic-release');
module.exports = semanticReleaseConfigDockerMulti({
  projectPath: __dirname
});
